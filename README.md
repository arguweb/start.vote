# Start.Vote

This is a reference implementation for the [Start.Vote](https://start.vote) voting process.

- [FAQ](https://start.vote/faq)
- [About the voting process](https://start.vote/process)

## How to run

Docker

1. `docker-compose up --build`
1. visit [`http://localhost/`]http://localhost/()

API / Backend

1. `cd api`
1. `yarn dev`
1. Visit [`localhost:3000`](http://localhost:3000).

Front-end

1. `cd front`
1. `yarn start`

## Data model

Various parts of this project uses the typescript models defined in `types.ts`.
