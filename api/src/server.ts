import express from 'express';
import { MongoClient } from 'mongodb';
import bodyParser from 'body-parser';
import cors from 'cors';
import routes from './routes/index';

import { dbName, url } from './config/db';

const app = express();

const port = 8000;

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
// Accept all cors headers
app.use(cors())

const mongoOpts = {
  useNewUrlParser: true,
}

MongoClient.connect(url, mongoOpts, (err, client: MongoClient) => {
  if (err) return console.log(err)

  const database = client.db(dbName);
  routes(app, database);
  app.listen(port, () => {
    console.log('We are live on ' + port);
  });
})
