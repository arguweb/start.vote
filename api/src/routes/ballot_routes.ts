import { Express, Request, Response } from 'express';
import { ObjectId, Db } from 'mongodb';
import { Ballot, BallotUnconfirmed, Vote, Option } from 'start-vote-types';

export function ballotRoutes(app: Express, db: Db) {
  // Retrieve single ballot
  app.get('/ballots/:id', (req: Request, res: Response) => {
    const details = { '_id': new ObjectId(req.params.id) };
    db.collection<Ballot>('ballots').findOne(details, (err, item) => {
      if (err) {
        res.status(500).send({'error': 'An error has occurred'});
      } else if (item == null) {
        res.status(404).send();
      } else if (item.options == undefined) {
        res.status(500).send({'error': 'No options present'});
      } else {
        const getVoteCountForOption = (optionId: number) => {
          return db.collection<Vote>('votes').find({
            'ballotId': req.params.id,
            'option': optionId.toString()
          }).count().then(r => {
            return r;
          });
        }

        item.totalVotes = 0;
        // Create optionCounts objects for every option
        const optionCounts = item.options.map(option =>
          getVoteCountForOption(option.id)
            .then(voteCount => {
              item.totalVotes += voteCount;
              return {
                title: option.title,
                id: option.id,
                count: voteCount,
              } as Option;
            })
        );

        Promise.all(optionCounts).then(counts => {
          item.optionCounts = counts;
          res.send(item)
        });
      }
    });
  });

  // Create new ballot
  app.post('/ballots', (req: Request, res: Response) => {
    const newBallot = req.body;
    db.collection<BallotUnconfirmed>('ballots').insertOne(newBallot, (err, result) => {
      if (err) {
        res.status(500).send({ 'error': err.message });
      } else {
        res.send(result.ops[0]);
      }
    });
  });

  // Retrieve single vote
  app.get('/ballots/:id/votes/:id', (req: Request, res: Response) => {
    const details = { '_id': new ObjectId(req.params.id) };
    db.collection<Vote>('votes').findOne(details, (err, vote) => {
      if (err) {
        res.status(500).send({'error': 'An error has occurred'});
      } else if (vote == null) {
        res.status(404).send();
      } else {
        res.send(vote);
      }
    });
  });

  // Create vote
  app.post('/ballots/:id/votes', (req: Request, res: Response) => {
    const newVote = req.body;
    const ballotSearch = {
      '_id': new ObjectId(newVote.ballotId),
    };
    // Check if the ballot exists and the hashedId is in it
    db.collection<Ballot>('ballots').findOne(ballotSearch, (err, ballot) => {
      if (err) {
        res.status(500).send({'error': 'An error has occurred'});
      } else if (ballot == null) {
        res.status(404).send({'error': 'Ballot not found'});
      } else if (ballot.doubleHashedIds.indexOf(newVote.doubleHashedId) === -1) {
        res.status(409).send({'error': 'VoteCode not in ballot'})
      } else if (ballot.doubleHashedIds.indexOf(newVote.doubleHashedId) > -1) {
        const voteSearch = {
          'doubleHashedId': newVote.doubleHashedId,
          'ballotId': newVote.ballotId,
        };
        // Check if the hashedId hasn't been used, post the vote
        db.collection<Vote>('votes').findOne(voteSearch, (err, vote) => {
          if (err) {
            res.status(500).send()
          } else if (vote != null) {
            res.status(409).send({'error': 'VoteCode already used'})
          } else if (vote == null) {
            db.collection('votes').insertOne(newVote, (err, result) => {
              if (err) {
                res.status(500).send({ 'error': 'An error has occurred' });
              } else {
                res.send(result.ops[0]);
              }
            });
          }
        });
      }
    });
  });
};
