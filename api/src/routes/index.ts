import { Express } from 'express';
import { Db } from 'mongodb';

import { ballotRoutes } from './ballot_routes';

export default function(app: Express, db: Db) {
  ballotRoutes(app, db);
};
