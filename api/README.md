# Start.Vote Node.JS + typescript back-end reference implementation
This back-end serves as a basic voting Service (see `../PROCESS.MD`). It offers a REST API to store and retrieve ballots.

## How to run

1. `cd back`
1. `yarn start`
