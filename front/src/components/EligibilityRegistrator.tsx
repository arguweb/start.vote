import * as React from 'react';
import { getAddressForAccount } from '../defetch/defetch';

interface MyComponentProps {
  eligibility: string;
  ballotId: string;
  username: string;
  password: string;
}

interface MyComponentState {
  username: string;
  password: string;
  publicAddress: string | null;
}

/** Submits an address and an eligibility code */
class EligibilityRegistrator extends React.Component<MyComponentProps, MyComponentState> {
  constructor(props: MyComponentProps) {
    super(props);

    this.state = {
      username: this.props.username,
      publicAddress: null,
      password: this.props.password,
    };

    this.handleChangeAddress = this.handleChangeAddress.bind(this);
  }

  handleChangeAddress(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      password: event.target.value,
    });
  }

  componentDidMount() {
    getAddressForAccount(this.props.username, this.props.password)
    .then(
      (address) => {
        this.setState({
          publicAddress: address,
        });
      }
    );
  }

  handleSubmitRegistration() {
    // https://argu.hybrix.io/engine/voting/addAddressRedirect/1234:myaddress/someWebsite
    // {data:"128371293"}
  }

  render() {
    return (
      <div>
        <label>Generated Username
          <input
            disabled={true}
            value={this.state.password}
            onChange={this.handleChangeAddress}
          />
        </label>
        <label>Generated Password
          <input
            disabled={true}
            value={this.state.username}
            onChange={this.handleChangeAddress}
          />
        </label>
        <label>Matching PublicAddress
          <p>{this.state.publicAddress}</p>
        </label>
        <button>
          Backup account
        </button>
        <a href="https://argu.hybrix.io/engine/voting/addAddressRedirect/1234:myaddress/someWebsite">
          Submit registration
        </a>
        <button
          onClick={this.handleSubmitRegistration}
        >
          Submit registration
        </button>
      </div>
    );
  }
}

export default EligibilityRegistrator;
