import * as React from 'react';
import jsSHA from 'jssha';
import classNames from 'classnames';

import { Ballot } from 'start-vote-types';
import NewVote from './NewVoteD';

interface MyComponentProps {
  ballot: Ballot;
}

interface MyComponentState {
  codeString: string;
  hashedCode: string;
  isOk: boolean;
}

/** Finds the hash for a string and checks whether it can vote */
class Hasher extends React.Component<MyComponentProps, MyComponentState> {
  constructor(props: MyComponentProps) {
    super(props);

    this.state = {
      codeString: '',
      hashedCode: '',
      isOk: false,
    };

    this.handleCheckHash = this.handleCheckHash.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.updateHash = this.updateHash.bind(this);
  }

  handleChange(e: React.SyntheticEvent<HTMLInputElement>) {
    this.setState(
      {
        codeString: e.currentTarget.value,
        hashedCode: this.state.hashedCode,
      },
      () => this.updateHash()
    );
  }

  handleCheckHash() {
    let matches = false;
    this.props.ballot.eligibilities.forEach(
      hash => {
        if (this.state.hashedCode === hash) {
          matches = true;
        }
      }
    );
    this.setState({
      isOk: matches,
    });
  }

  updateHash() {
    const shaObj = new jsSHA('SHA-512', 'TEXT');
    shaObj.update(this.props.ballot.ballotId + this.state.codeString);
    this.setState(
      {
        codeString: this.state.codeString,
        hashedCode: shaObj.getHash('HEX'),
      },
      () => this.handleCheckHash()
    );
  }

  render() {
    const classes = classNames({
      'Hasher__hash': true,
      'Hasher__hash--ok': this.state.isOk,
    });

    return (
      <div className="Hasher">
        <label>Enter your VoteCode:</label>
        <input
          className="Hasher__input"
          type="password"
          onChange={this.handleChange}
        />
        {this.state.codeString !== '' &&
          <div className="Hasher__validator">
            {this.state.isOk && <div>Valid code!</div>}
            {!this.state.isOk && <div>Not a valid code.</div>}
            <span className={classes}>
              {this.state.hashedCode}
            </span>
          </div>
        }
        {this.state.isOk &&
        <NewVote
          ballot={this.props.ballot}
          hashedId={this.state.hashedCode}
        />}
      </div>
    );
  }
}

export default Hasher;
