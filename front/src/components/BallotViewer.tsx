import * as React from 'react';
import {
  Ballot,
} from 'start-vote-types';

interface MyComponentProps {
  ballot: Ballot;
}

interface OptionCompProps {
  title: string;
  count: number;
}

const OptionComp = (props: OptionCompProps) => (
  <li>
    <span className="Ballot__option">{props.title}</span>
    <span className="Ballot__option__voted"> ({props.count}x voted)</span>
  </li>
);

class BallotViewer extends React.PureComponent<MyComponentProps> {
  render() {
    return (
      <div>
        <h1>
          {this.props.ballot.title}
        </h1>
        <p>
          {this.props.ballot.description}
        </p>
        <p>options:</p>
        <ul className="list">
          {this.props.ballot.optionCounts.map(
            item =>
            <OptionComp
              title={item.title}
              count={item.count}
              key={`${item.title}${item.id}`}
            />
          )}
        </ul>
        <p className="deadline">
          Voting closes on {new Date(this.props.ballot.deadline).toLocaleDateString()}
        </p>
      </div>
    );
  }
}

export default BallotViewer;
