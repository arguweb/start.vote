import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Ballot, OptionUnconfirmed } from 'start-vote-types';
import { APIurl, headers } from '../shared';

interface NewVoteProps extends RouteComponentProps<never> {
  ballot: Ballot;
  hashedId: string;
}

interface NewVoteState {
  option?: number;
  responseCode?: number;
  error?: Error;
}

class NewVote extends React.Component<NewVoteProps, NewVoteState> {
  constructor(props: NewVoteProps) {
    super(props);

    this.state = {
    };

    this.handleSetOption = this.handleSetOption.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSetOption(event: React.ChangeEvent<any>) {
    this.setState({
      option: event.currentTarget.id,
    });
  }

  async handleSubmit(event: React.FormEvent<never>) {
    event.preventDefault();
    const response = await fetch(`${APIurl}ballots/${this.props.ballot._id}/votes`, {
      method: 'POST',
      headers,
      body: JSON.stringify({
        option: this.state.option,
        doubleHashedId: this.props.hashedId,
        ballotId: this.props.ballot._id,
      })
    });

    const json = await response.json();

    if (json.error !== undefined) {
      this.setState({
        responseCode: response.status,
        error: json.error,
      });
    } else if (json._id) {
      this.props.history.push(`${this.props.ballot._id}/votes/${json._id}`);
    }

    this.setState({
      responseCode: response.status,
      error: json.error,
    });
  }

  optionComponent(option: OptionUnconfirmed) {
    return (
      <label
        className="NewVote__option"
        key={option.id}
      >
        <input
          type="radio"
          className="NewBallot__option"
          value={option.title}
          name={'option'}
          id={`${option.id}`}
          onChange={this.handleSetOption}
        />
        {option.title}
      </label>
    );
  }

  voteOptions() {
    return (
      this.props.ballot.options.map(
        option =>
          this.optionComponent(option)
      )
    );
  }

  render() {
    if (this.state.responseCode) {
      return (
        <p>{this.state.error}</p>
      );
    }
    return (
      <form onSubmit={this.handleSubmit} >
        <label>
          <span className="label_span">
            Options:
          </span>
          {this.voteOptions()}
        </label>
        {this.state.option &&
          <input type="submit" value="Submit" />
        }
      </form>
    );
  }
}

export default withRouter(NewVote);
