import * as React from 'react';
import { getAddress } from '../defetch/defetch';

interface MyComponentProps {
  inputString: string;
}

interface MyComponentState {
  generatedAddress?: string;
  loading: boolean;
}

/** Finds the hash for a string and checks whether it can vote */
class Address extends React.Component<MyComponentProps, MyComponentState> {
  constructor(props: MyComponentProps) {
    super(props);

    this.state = {
      loading: false,
    };

    this.handleGenerateAddress = this.handleGenerateAddress.bind(this);
  }

  componentDidMount() {
    return this.handleGenerateAddress();
  }

  handleGenerateAddress() {
    this.setState({
      loading: true,
    });

    return getAddress(this.props.inputString)
    .then((address) => {
      this.setState({
        generatedAddress: address,
      });
    })
    .finally(() => {
      this.setState({
        loading: false,
      });
    });
  }

  render() {
    return (
      <div className="Balance">
        {this.state.loading && 'loading...'}
        {this.state.generatedAddress && this.state.generatedAddress}
      </div>
    );
  }
}

export default Address;
