import * as React from 'react';
import {
  Ballot,
} from 'start-vote-types';

import BallotViewer from './BallotViewer';
import Hasher from './Hasher';
import { APIurl } from '../shared';

interface MyComponentProps {
  id: string;
}

interface MyComponentState {
  ballot?: Ballot;
}

class BallotContainer extends React.Component<MyComponentProps, MyComponentState> {
  constructor(props: MyComponentProps) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    fetch(`${APIurl}ballots/${this.props.id}`, {
      method: 'GET',
      body: JSON.stringify(this.state.ballot)
    }).then(response => {
      if (response.status === 200) {
        return response.json();
      }
      return undefined;
    }).then(json => {
      this.setState({
        ballot: json
      });
   });
  }

  render() {
    if (this.state.ballot === undefined) {
      return null;
    }
    return (
      <div>
        <BallotViewer ballot={this.state.ballot}/>
        <Hasher ballot={this.state.ballot} />
      </div>
    );
  }
}

export default BallotContainer;
