import * as React from 'react';
import { Vote } from 'start-vote-types';

export interface SingleVoteProps {
  vote: Vote;
}

class SingleVote extends React.Component<SingleVoteProps> {
  render() {
    return (
      <div className="SingleVote">
        <div>You voted: {this.props.vote.option}</div>
        <div>Your doubleHashedId is:
          <div
            className="Hasher__hash"
          >
            {this.props.vote.doubleHashedId}
          </div>
        </div>
        <div>You voted on: {this.props.vote.createdAt}</div>
      </div>
    );
  }
}

export default SingleVote;
