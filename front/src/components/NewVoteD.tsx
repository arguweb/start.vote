import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Ballot, OptionUnconfirmed } from 'start-vote-types';
import { sendVote, checkBalance } from '../defetch/defetch';

interface NewVoteProps extends RouteComponentProps<never> {
  ballot: Ballot;
  hashedId: string;
}

interface NewVoteState {
  option?: number;
  responseCode?: number;
  error?: Error;
  success?: any;
}

class NewVoteD extends React.Component<NewVoteProps, NewVoteState> {
  constructor(props: NewVoteProps) {
    super(props);

    this.state = {
    };

    this.handleSetOption = this.handleSetOption.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCheckBalance = this.handleCheckBalance.bind(this);
  }

  handleSetOption(event: React.ChangeEvent<any>) {
    this.setState({
      option: event.currentTarget.id,
    });
  }

  async handleCheckBalance(event: React.FormEvent<any>) {
    checkBalance();
  }

  async handleSubmit(event: React.FormEvent<never>) {
    event.preventDefault();

    const onSuccess = (data) => {
      this.setState({
        success: data,
      });
    };

    const onError = (error) => {
      this.setState({
        error: error,
      });
    };

    sendVote('targetExample', onSuccess, onError);
  }

  optionComponent(option: OptionUnconfirmed) {
    return (
      <label
        className="NewVote__option"
        key={option.id}
      >
        <input
          type="radio"
          className="NewBallot__option"
          value={option.title}
          name={'option'}
          id={`${option.id}`}
          onChange={this.handleSetOption}
        />
        {option.title}
      </label>
    );
  }

  voteOptions() {
    return (
      this.props.ballot.options.map(
        option =>
          this.optionComponent(option)
      )
    );
  }

  render() {
    if (this.state.responseCode) {
      return (
        <p>{this.state.error}</p>
      );
    }
    return (
      <form onSubmit={this.handleSubmit} >
        <button onClick={this.handleCheckBalance}>Check balance</button>
        <label>
          <span className="label_span">
            Options:
          </span>
          {this.voteOptions()}
        </label>
        {this.state.option &&
          <input type="submit" value="Submit" />
        }
        {this.state.success &&
          <p>{this.state.success}</p>
        }
        {this.state.error &&
          <p>Error: {this.state.error}</p>
        }
      </form>
    );
  }
}

export default withRouter(NewVoteD);
