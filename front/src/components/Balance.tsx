import * as React from 'react';
import { checkBalance } from '../defetch/defetch';

interface MyComponentProps {
}

interface MyComponentState {
  balance?: string;
  progress?: number;
  error?: any;
}

/** Finds the hash for a string and checks whether it can vote */
class Balance extends React.Component<MyComponentProps, MyComponentState> {
  constructor(props: MyComponentProps) {
    super(props);

    this.state = {
    };

    this.handleCheckBalance = this.handleCheckBalance.bind(this);
  }

  async handleCheckBalance(event: React.FormEvent<any>) {
    event.preventDefault();

    const onSuccess = (data) => {
      this.setState({
        balance: data,
      });
    };

    const onError = (error) => {
      this.setState({
        error: error,
      });
    };

    const onProgress = (percent) => {
      this.setState({
        progress: percent,
      });
    };

    checkBalance(onSuccess, onError, onProgress);
  }

  render() {
    return (
      <div className="Balance">
        <button onClick={this.handleCheckBalance}>
          Check vote tokens
        </button>
        {this.state.progress && this.state.progress < 1 && `${this.state.progress.toPrecision(2)}%`}
        {this.state.balance && this.state.balance}
      </div>
    );
  }
}

export default Balance;
