import {
  Ballot,
  BallotUnconfirmed,
} from 'start-vote-types';

export const ballotUnconfirmedDemo: BallotUnconfirmed = {
  title: 'We should do more with blockchain.',
  ballotId: 'someId',
  description: 'This is a demo ballot. Use one the following strings to vote: \'joep\' \'thom\'.',
  options: [
    {
      id: 0,
      title: 'In favor'
    },
    {
      id: 1,
      title: 'Against'
    }
  ],
  eligibilities: [
    '20042526370653befa524fafb6664b6202e2c46b37a7586844a2ee148383fb5a7f7f4051'
    + 'b4db0f2122b91cb6a06d507f79e7ab9ea76c173d26c5a20a1ed6f3f2',
    '214db516c489b632feef429e9011789498877aae1c272120e61a4a79db26277af3c9629ba'
    + '1e0cda87d83b7381881851a18880c91aaa731fd6b12935e7f750ecc',
    '6b861e8c745dcf5df78b61c9b8c0fe7edc06965983f77ea4381ceb25dcf26114895ed1'
    + '71b20ac477e7a1edbe29c5ad9c2cfba2805394cffd8ab2e3c1715439c',
  ],
  deadline: new Date().toISOString(),
};

export const ballotDemo: Ballot = {
  ...ballotUnconfirmedDemo,
  url: 'https://start.vote',
  _id: '5a8c4b2e09c09867ed8ec093',
  totalVotes: 125,
  optionCounts: [
    {
      id: 0,
      _id: 'gapsemgaoingeo',
      title: 'In favor',
      count: 5
    },
    {
      id: 1,
      _id: '31508h15h850h1',
      title: 'Against',
      count: 2,
    }
  ],
};
