export default function createRandomHex (length=497) {
  let text = "";
  const possible = "0123456789ABCDEF";

   for (let i = 0; i < length; i++) {
     text += possible.charAt(Math.floor(Math.random() * possible.length));
   }

  return text;
}
