import * as React from 'react';
import './reset.css';
import './App.css';
import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
} from 'react-router-dom';

import NewBallotRoute from './routes/NewBallotRoute';
import ViewBallotRoute from './routes/ViewBallotRoute';
import ViewVoteRoute from './routes/ViewVoteRoute';
import Register from './routes/Register';

class App extends React.Component {
  render() {
    return (
      <Router>
        <div
          className="App"
        >
          <Link to="/new">New Ballot</Link>
          <Link to="/5b746c381fe4f10f6c252428">Example Ballot</Link>
          <Link to="/5b746c381fe4f10f6c252428/register/gosienggeegssegse">Example Register</Link>
          <Switch>
            <Route exact={true} path="/" component={NewBallotRoute} />
            <Route path="/new" component={NewBallotRoute} />
            <Route path="/:ballotId/votes/:voteId" component={ViewVoteRoute} />
            <Route path="/:ballotId/register/:eligibility" component={Register} />
            <Route path="/:id" component={ViewBallotRoute} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
