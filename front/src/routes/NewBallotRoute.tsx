import { withRouter, RouteComponentProps } from 'react-router-dom';
import * as React from 'react';
import jsSHA from 'jssha';

import {
  BallotUnconfirmed,
  OptionUnconfirmed,
} from 'start-vote-types';
import { APIurl, headers } from '../shared';
import { ballotUnconfirmedDemo } from '../demoData';
import Balance from '../components/Balance';
import Address from '../components/Address';
import createRandomHex from '../helpers/random';
import convertToHex from '../helpers/convertToHex';
import { addEligibilities, closeEligibility } from 'src/defetch/defetch';

interface NewBallotProps extends RouteComponentProps<never> {
}

interface NewBallotState {
  ballot: BallotUnconfirmed;
  newOption: OptionUnconfirmed;
  newVoteCode: {
    title: string,
    address: string,
    id: number,
  };
}

class NewBallotRoute extends React.Component<NewBallotProps, NewBallotState> {
  constructor(props: NewBallotProps) {
    super(props);

    this.state = {
      ballot: {
        description: '',
        title: '',
        options: [],
        eligibilities: [],
        deadline: ballotUnconfirmedDemo.deadline,
        // Todo: find a way to create more entropy
        ballotId: createRandomHex()
      },
      newOption: {
        id: 0,
        title: '',
      },
      newVoteCode: {
        id: 0,
        title: '',
        address: '',
      },
    };

    this.handleSubmitEligibilityCodes = this.handleSubmitEligibilityCodes.bind(this);
    this.handleCloseEligibilityCodes = this.handleCloseEligibilityCodes.bind(this);
    this.handleAddOption = this.handleAddOption.bind(this);
    this.handleAddVoteCode = this.handleAddVoteCode.bind(this);
    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleChangeNewOption = this.handleChangeNewOption.bind(this);
    this.handleChangeNewVoteCode = this.handleChangeNewVoteCode.bind(this);
    this.handleChangeNewVoteCodeAddress = this.handleChangeNewVoteCodeAddress.bind(this);
    this.handleDownloadEligibilityCodes = this.handleDownloadEligibilityCodes.bind(this);
    this.handleRemoveOption = this.handleRemoveOption.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // TODO: actually generate the address
  createAddress(name: string) {
    if (name === '') {
      return '';
    }
    const shaObj = new jsSHA('SHA-512', 'TEXT');

    /**
     * Create a unique string with high entropy,
     * Even if the name is not unique
     */
    shaObj.update(this.state.ballot.ballotId + name);
    return shaObj.getHash('HEX');
  }

  handleChangeTitle(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      ballot: {
        ...this.state.ballot,
        title: event.target.value,
      }
    } as NewBallotState);
  }

  handleChangeDescription(event: React.ChangeEvent<HTMLTextAreaElement>) {
    this.setState({
      ballot: {
        ...this.state.ballot,
        description: event.target.value,
      }
    } as NewBallotState);
  }

  handleChangeNewOption(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      newOption: {
        ...this.state.newOption,
        title: event.target.value,
        address: this.createAddress(event.target.value),
      }
    } as NewBallotState);
  }

  handleChangeNewVoteCodeAddress(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      newVoteCode: {
        ...this.state.newVoteCode,
        title: '',
        address: event.target.value,
      }
    } as NewBallotState);
  }

  handleChangeNewVoteCode(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      newVoteCode: {
        ...this.state.newVoteCode,
        title: event.target.value,
        address: this.createAddress(event.target.value),
      }
    } as NewBallotState);
  }

  handleAddOption(event: React.FormEvent<never>) {
    event.preventDefault();
    this.setState({
      newOption: {
        id: this.state.newOption.id + 1,
        title: '',
      },
      ballot: {
        ...this.state.ballot,
        options: this.state.ballot.options.concat(this.state.newOption),
      },
    } as NewBallotState);
  }

  handleAddVoteCode(event: React.FormEvent<never>) {
    event.preventDefault();
    this.setState({
      newVoteCode: {
        id: this.state.newVoteCode.id + 1,
        title: '',
        address: '',
      },
      ballot: {
        ...this.state.ballot,
        eligibilities: this.state.ballot.eligibilities.concat(this.state.newVoteCode.address),
      },
    });
  }

  handleSubmit(event: React.FormEvent<never>) {
    event.preventDefault();
    fetch(`${APIurl}ballots`, {
      method: 'POST',
      headers,
      body: JSON.stringify(this.state.ballot)
    }).then(response => {
      if (response.status === 200) {
        response.json().then(
          json => this.props.history.push(json._id)
        );
      } else {
        throw Error;
      }
    }
    );
  }

  handleSubmitEligibilityCodes(event: React.MouseEvent<HTMLElement>) {
    event.preventDefault();
    const ballotId = this.state.ballot.ballotId || createRandomHex();
    const codes = 'code1,code2';
    const onSuccess = successHash => window.alert(successHash);
    addEligibilities(ballotId, codes, onSuccess);
  }

  handleCloseEligibilityCodes(event: React.MouseEvent<HTMLElement>) {
    event.preventDefault();
    const ballotId = this.state.ballot.ballotId;
    if (ballotId !== undefined) {
      closeEligibility(ballotId);
    }
  }

  handleDownloadEligibilityCodes(event: React.MouseEvent<HTMLElement>) {
    event.preventDefault();

    let csvStart = 'data:text/csv;charset=utf-8,';
    csvStart += this.state.ballot.eligibilities.join(',');
    console.log(csvStart);
    const encodedUri = encodeURI(csvStart);

    window.open(encodedUri);
  }

  handleRemoveOption(event: any) {
    const id = Number(event.target.parentElement.id);
    this.setState({
      ballot: {
        ...this.state.ballot,
        options: this.state.ballot.options.filter((item => item.id !== id))
      }
    } as NewBallotState);
  }

  optionComponent(option: OptionUnconfirmed) {
    return (
      <li
        className="NewBallot__option"
        key={option.id}
        id={`${option.id}`}
        title={option.address}
      >
        <button
          className="Button Button--round Button--subtle NewBallot__option__remove"
          onClick={this.handleRemoveOption}
        >
          x
        </button>
        {option.title}
        <div className="Hasher__hash">
          <Address inputString={convertToHex(option.title) + this.state.ballot.ballotId}/>
        </div>
      </li>
    );
  }

  voteCodes() {
    return (
      this.state.ballot.eligibilities.map(
        (code, i) => (
          <div
            className="Hasher__hash"
            key={i}
          >
          {code}
          </div>
        )
      )
    );
  }

  voteOptions() {
    return (
      this.state.ballot.options.map(
        option =>
          this.optionComponent(option)
      )
    );
  }

  addOptionForm() {
    return (
      <form onSubmit={this.handleAddOption} className="Form_input-with-button">
        <input
          className="Input Input--with-button"
          type="text"
          value={this.state.newOption.title}
          onChange={this.handleChangeNewOption}
          placeholder="New vote option ..."
        />
        <input
          disabled={this.state.newOption.title === ''}
          type="submit"
          value="add"
          className="Button Button--inline-form NewBallot__add-option__button"
        />
      </form>
    );
  }

  addVoteCodeForm() {
    return (
      <form onSubmit={this.handleAddVoteCode} className="Form_input-with-button">
        <input
          className="Input Input--with-button"
          type="password"
          value={this.state.newVoteCode.title}
          onChange={this.handleChangeNewVoteCode}
          placeholder="Enter secret VoteCode"
        />
        <input
          className="Input Input--with-button"
          type="text"
          value={this.state.newVoteCode.address}
          onChange={this.handleChangeNewVoteCodeAddress}
          placeholder="Generated address"
        />
        <input
          disabled={this.state.newVoteCode.title === ''}
          type="submit"
          value="add"
          className="Button Button--inline-form NewBallot__add-option__button"
        />
      </form>
    );
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit} id="newBallot" >
          <label>
            <span className="label_span">
              Title:
            </span>
            <input
              required={true}
              className="input-title"
              type="text"
              value={this.state.ballot.title}
              onChange={this.handleChangeTitle}
            />
          </label>
          <label>
            <span className="label_span">
              Description:
            </span>
            <textarea
              rows={4}
              className="textarea"
              value={this.state.ballot.description}
              onChange={this.handleChangeDescription}
              placeholder="Optional"
            />
          </label>
          <label>
            <span className="label_span">
              Random seed:
            </span>
            <div className="label_helper">
              This is used to make sure that the addresses created by the Options and Voter Addresses are truly unique.
            </div>
            <input
              disabled={true}
              required={true}
              type="text"
              value={this.state.ballot.ballotId}
              onChange={this.handleChangeTitle}
            />
          </label>
        </form>
        <label>
          <span className="label_span">
            Options:
          </span>
        </label>
        <ul>
          {this.voteOptions()}
          {this.addOptionForm()}
        </ul>
        <Balance/>
        <label>
          <span className="label_span">
            Eligibilty Codes:
          </span>
        </label>
        <ul>
          {this.voteCodes()}
          {this.addVoteCodeForm()}
        </ul>
        {
          this.state.ballot.eligibilities &&
          <button
            className="Button"
            type="button"
            onClick={this.handleSubmitEligibilityCodes}
          >
            Submit Eligibility Codes
          </button>
        }
        <button
          className="Button"
          type="button"
          onClick={this.handleCloseEligibilityCodes}
        >
          Close Eligibility Codes
        </button>
        <button
          className="Button"
          type="button"
          onClick={this.handleDownloadEligibilityCodes}
        >
          Downlload Eligibility Codes (csv)
        </button>
        <input
          disabled={this.state.ballot.options.length === 0}
          className="Button"
          type="submit"
          value="Submit"
          form="newBallot"
        />
      </div>
    );
  }
}

export default withRouter(NewBallotRoute);
