import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { createAccount } from 'src/defetch/defetch';
import EligibilityRegistrator from 'src/components/EligibilityRegistrator';

interface Params {
  ballotId: string;
  eligibility: string;
}

interface RegisterProps extends RouteComponentProps<Params> {}

interface RegisterState {
  isGenerating: boolean;
  username?: string;
  password?: string;
  error?: string;
}

class RegisterRoute extends React.Component<RegisterProps, RegisterState> {
  constructor(props: RegisterProps) {
    super(props);

    this.state = {
      isGenerating: false,
    };
  }

  componentDidMount() {
    this.createKeyPair();
  }

  createKeyPair() {
    this.setState(
    {
      isGenerating: true,
    },
    () => {
      createAccount()
      .then(
        (data) => this.setState(
          {
            isGenerating: false,
            username: data.userid,
            password: data.passwd,
          }
        )
      ).catch(
        (e) => this.setState(
          {
            isGenerating: false,
            error: e.toString() || 'Something went wrong!',
          }
        )
      );
    });
  }

  render() {
    const ballotId = this.props.match.params.ballotId;
    const eligibility = this.props.match.params.eligibility;

    return (
      <div>
        <p>ballotId: {ballotId}</p>
        <p>eligibily: {eligibility}</p>
        {this.state.isGenerating &&
          <p>Creating your secret code...</p>
        }
        {this.state.error &&
          <span>{this.state.error}</span>
        }
        {this.state.username && this.state.password &&
          <EligibilityRegistrator
            ballotId={ballotId}
            eligibility={eligibility}
            username={this.state.username}
            password={this.state.password}
          />
        }
      </div>
    );
  }
}

export default withRouter(RegisterRoute);
