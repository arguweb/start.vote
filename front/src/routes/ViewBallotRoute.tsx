import * as React from 'react';
import { match } from 'react-router';

import BallotContainer from '../components/BallotContainer';

interface ViewBallotRouteProps {
  match: match<any>;
}

class ViewBallotRoute extends React.Component<ViewBallotRouteProps> {
  render() {
    return (
      <BallotContainer id={this.props.match.params.id}/>
    );
  }
}

export default ViewBallotRoute;
