import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import {
  Vote,
} from 'start-vote-types';
import { APIurl, headers } from '../shared';
import BallotContainer from '../components/BallotContainer';
import SingleVoteComp from '../components/SingleVote';

interface Params {
  ballotId: string;
  voteId: string;
}

interface VoteProps extends RouteComponentProps<Params> {
  params: Params;
}

interface VoteState {
  vote?: Vote;
}

class ViewVoteRoute extends React.Component<VoteProps, VoteState> {
  constructor(props: VoteProps) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    fetch(`${APIurl}ballots/${this.props.match.params.ballotId}/votes/${this.props.match.params.voteId}`, {
      method: 'GET',
      headers,
    }).then(response => {
      if (response.ok) {
        response.json().then(
          json => this.setState({
            vote: json
          })
        );
      }
    });
  }

  render() {
    if (this.state.vote) {
      return (
        <div>
          <SingleVoteComp vote={this.state.vote} />
          <BallotContainer id={this.state.vote.ballotId} />
        </div>
      );
    }
    return null;
  }
}

export default ViewVoteRoute;
