// Defetch contains all functions that handle with the decentralized library, i.e. Hybrix

import * as Hybrix from '../lib/hybrix-lib.nodejs';
import * as nacl_factory from 'js-nacl';

import credentials from '../credentials';
import createRandomHex from '../helpers/random';

(window as any).nacl_factory = nacl_factory;

const mainsession = new Hybrix.Interface({XMLHttpRequest: XMLHttpRequest});

interface ConnectionData {
  username: string;
  password: string;
}

const defaultConnectionData = {
  username: credentials.username || '3MNY6P42EYFPIVHW',
  password: credentials.password || '6AAOR4FMKF6VB5E2RE4ORUW7XPUUTSO7RNCZ2SOASWO47F6Y',
};

const logDefault = (item) => console.log(item);
const errorDefault = (e) => console.log(e);
const progressDefault = (percent) => console.log(percent);

const symbol = 'waves.vote';
// const host = 'http://wallet-uat.internetofcoins.org/api/';
const host = 'https://argu.hybrix.io/';
// const host = 'http://127.0.0.1:1111/';

// Initialize connection
openConnection();

export function openConnection(
  data: ConnectionData = defaultConnectionData,
  onSuccess: Function = logDefault,
  onError: Function = logDefault,
  onProgress: Function = logDefault,
): void {
  mainsession.sequential(
    [
      'init',
      data, 'session',
      { host }, 'addHost',
      { symbol: 'waves.vote' }, 'addAsset'
    ],
    onSuccess,
    onError,
    onProgress,
    0,
    0
  );
}

// Create Address with new account
export function getAddress(
  // HEX only, min 491 chars
  inputString: String,
  onProgress: Function = progressDefault,
): Promise<string> {
  return new Promise((resolve, reject) => {
    let generatedAddress = '';

    mainsession.sequential(
      [
        // Create new session and account to use entropy from inputString
        { entropy: inputString }, 'createAccount',
        'session',
        { symbol: 'waves.vote' }, 'addAsset',
        { symbol }, 'getAddress',
        (address: string) => { generatedAddress = address; },
        defaultConnectionData, 'session',
      ],
      () => resolve(generatedAddress),
      reject,
      onProgress,
      0,
      0
    );
  });
}

export function checkBalance(
  onSuccess: Function = logDefault,
  onError: Function = errorDefault,
  onProgress: Function = progressDefault,
) {
  mainsession.sequential(
    [
      { symbol }, 'getAddress',
      (address) => {
      return {
        query: `asset/waves.vote/balance/${address}`
      };
    },
      'rout'
    ],
    onSuccess,
    onError,
    onProgress,
    0,
    0,
  );
}

export function sendVote(
  // The address of the selected Vote Option
  target: String,
  onSuccess: Function = logDefault,
  onError: Function = errorDefault,
  onProgress: Function = progressDefault,
) {
  const amount = 1;
  mainsession.sequential(
    [
      { symbol, amount, target }, 'rawTransaction'
    ],
    onSuccess,
    onError,
    onProgress,
    0,
    0,
  );
}

export function addEligibilities(
  // String without slashes
  ballotId: String,
  // Comma seperated list of EligibilityCodes, max 100?
  codes: String,
  onSuccess: Function = logDefault,
  onError: Function = errorDefault,
  onProgress: Function = progressDefault,
) {
  mainsession.sequential(
    [
      { query: `/e/voting/addEligibility/${ballotId}/${codes}`}, 'rout'
    ],
    onSuccess,
    onError,
    onProgress,
    0,
    0,
  );
}

export function closeEligibility(
  // String without slashes
  ballotId: String,
  onSuccess: Function = logDefault,
  onError: Function = errorDefault,
  onProgress: Function = progressDefault,
) {
  mainsession.sequential(
    [
      { query: `/e/voting/closeEligibility/${ballotId}`}, 'rout'
    ],
    onSuccess,
    onError,
    onProgress,
    0,
    0,
  );
}

interface CreateAccountData {
  userid: string;
  passwd: string;
}

export function createAccount(): Promise<CreateAccountData> {
  return new Promise((resolve, reject) => {
    mainsession.sequential(
      [
        'init',
        {
          entropy: createRandomHex(492)
        }, 'createAccount',
      ],
      (data: CreateAccountData) => resolve(data),
      reject,
      null,
      0,
      0
    );
  });
}

// Create Address with existing account
export function getAddressForAccount(
  username: String,
  password: String,
): Promise<string> {
  return new Promise((resolve, reject) => {
    let generatedAddress = '';

    mainsession.sequential(
      [
        'init',
        {
          username,
          password,
        }, 'session',
        { symbol: 'waves.vote' }, 'addAsset',
        { symbol }, 'getAddress',
        (address: string) => { generatedAddress = address; },
        defaultConnectionData, 'session',
      ],
      () => resolve(generatedAddress),
      reject,
      progressDefault,
      0,
      0
    );
  });
}
