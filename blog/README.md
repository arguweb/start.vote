# Webpage & blog for Start.Vote
This project runs on Jekyll.

## Run
* Install [Ruby](https://www.ruby-lang.org/en/documentation/installation/)
* `bunlde install`
* `bunlde exec jekyll serve`
* Visit [`localhost:4000`](http://localhost:4000)
