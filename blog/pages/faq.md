---
layout: narrow
title: FAQ
description: We're linked data web development specialists that can help you improve your data infrastructure.
permalink: /faq
---

## Should we use Start.Vote for our national elections?

No.
All digital voting systems, as far as we know, have some important drawbacks that make them unsuitable for national elections,
especially when you have to prove your identity from behind your computer or smartphone.
These problems are fundamental to all voting systems that require only a form of online identification.
Firstly, someone could coerce (e.g. threaten) a voter to vote for something.
Secondly, a voter could be tempted to sell the vote rights to someone else.
Finally, hacking attempts by foreign governments could have an effect on
We can't solve these problems, but we can solve other existing problems with online voting.

## Why vote electronically at all?

Electronic voting can be faster, easier and cheaper than voting with pen and paper.
Automation removes the time-consuming chore of counting votes manually.
When you vote electronically using the internet (i.e. online voting), people can vote at any time and place, which makes it far more accessible and easy to use.
When we want to vote on things regularly, we need voting to be as easy as possible.

## What’s wrong with centralized online voting systems?

Any centralized online voting system has a single point of failure that can be hacked or manipulated.
This can be, for example, the server that stores the votes or the software that does the counting.
The central authority that hosts the voting process has to store the vote data and count the votes.
Both the data and the counting process could be tempered with by some malicious actor, like a hacker or a corrupt employee.
This is why we need to store the information decentralized

## Why use distributed ledger technology (e.g. blockchain) for voting?

Distributed ledger technology solves a fundamental problem: it guarantees that multiple machines arrive at the same conclusion about which transactions (or votes) are valid and which are not.
Individual transactions are validated across multiple machines, and the resulting data is stored decentralized as well.
Becose of this decentralized validation and storage of the tranasctions, it becomes virtually impossible to temper with.
Since anyone can read this data, the process is transparent and fully verifiable.
Besides being transparent, distributed ledgers are _immutable_, which means that data can only be added, but can never be changed.
Another advantage of decentralized systems is that DDOS attacks are harder to perform.
These characteristics make distributed ledgers very useful for financial transactions, which is why cryptocurrencies such as bitcoin have risen in popularity.

These same advantages that distributed ledgers have for financial transactions, are also valuable for voting processes.
The secure and transparent nature of these systems make it harder to temper with votes, and make it easier to verify the validity of the outcome.

## How can I trust Start.Vote?

The software runs entirely in your own browser, instead of some proprietary machine. You can check the source code, you can verify what happens to your data. You can use your network inspector to verify that your VoteCode is never sent. You can’t do that with special machines that are used in elections.

## What does the name Start.Vote mean?

**S**ecure, **T**ransparent, **A**nonymous, **R**eliable, **T**ransaction-based voting! But to be honest, the name predates the acronym 😉.

## What makes Start.Vote different from other blockchain voting projects?

Two things: our blockchain-agnostic transaction based design and our decentralized invitation system.

## What do you mean by blockchain-agnostic?

We did not want to rely on one specific network or decentralized ledger, so Argu partnered up with Internet of Coins to use their Hybridd technology. As you can see in the repository, the Hybridd interface can be used with various cryptographic assets.

Although our design is not restricted to one asset, we currently only support the Waves chain due to the relatively low transaction costs.

## How does your voting process work?

There are a couple of ways to use Start.Vote, each with different degrees of security and anonimity. The most secure way is to let voters register by creating addresses on some chain to where the vote codes can be sent. These addresses are sent through a swarm of nodes that remove the identity from

The user has to register as a voter and create a key-pair. The public key is sent to the vote organizer. This public key should not be linked to the user, so its sent

## How do you make the vote anonymous?

The anonimization happens when the Vote addresses are sent to the voters by the swarm.
In other words, nodoby has a mapping of which e-mail address is linked to a coin address.
This means that even _the organizer_ does not know which public address belongs to which user.

## Who sends the e-email invites to the voters?

This is done by "the swarm": a group of computers running Hybridd nodes. In other words, the mailing process is decentralized. There is not a single server sending all the invitations. Due to this decentralization, there is not a single entity that knows the entire mapping of e-mails and addresses.

## Can people sell or trade their votes?

Since we use existing cryptocurrencies for sending the vote tokens, selling a vote token is technically possible. However, since all transactions are traceable, anybody can verify whether a vote has been transacted on the network. The Start.Vote counting software ignores votes that have been traded.
However, users can still sell their unique personal VoteCode, since that string gives the right to place a vote.

## What harm can malicious nodes do?

A malicious node is a computer that runs an edited and harmful version of the decentralized software that powers Start.Vote.
Since our software mostly runs on HybridD nodes, we mean

- A malicious node could quietly save the link the Voter Address to the Eligibility Code. On it's own, this information does not show who voted what, but if the owner of this malicious node _also_ has access to how the Eligibility Codes are linked to voters, he could find out who voted for what.
- During voter registration, a malicious node could intercept the submitted vote address and change it to something else.
- Register the IP address of the voter, coupled to the transaction. Although the address itself is anoymous, the IP address is not. To prevent this, the voter could vote using a VPN or the Tor network.

## How do you protect against malicious nodes?

The nodes run so called ‘quartz recipies’, which are executed multiple times, on several machines. If there is no consensus, the recipe is executed again on a new, random set of machines in the swarm. There has to be a significant part of nodes that is malicious to let this happen.

## How do you prevent the bandwagon effect of polling in a public chain?

The bandwagon effect refers to the phenomenon where people are more likely to vote on expected winners during elections. In an election where all votes are transparent, anybody can see at any time who is winning. This means that the bandwagon effect is hard to
We don’t, but it is possible. We could create a set of addresses for each voting option, keep them secret for some time and make them public after the vote.

## Can you protect against coercion?

Coercion can happen when someone is threatened to vote for some specific candidate.
Contrary to paper voting, Start.Vote allows voters to verify that their votes are counted and stored correctly
This also means that this verification can be done in the presence of some coercer.
Since we want votes to be verifyable, we can't protect against coercion.

## How could this project be improved?

<!-- TODO @agent725 -->
Add layers to eligibilitycodes / mixmaster

## Can you customize your voting tool or process?

Sure, we can develop features on demand. [Get in touch](/contact)!

## What harm could a malicious or infected Organizer do?

- The Organizer knows all the e-mails of the receipients.
- Become part of the swarm, know part of the votes. How to solve: run many nodes / run your own node!

## Who's behind this?

Start.Vote is a collaboration by [Argu.co](https://argu.co) and [Internet of Coins](https://internetofcoins.org). Argu is an e-democracy platform, where people can share, discuss and vote on ideas. Internet of Coins is a non-profit platform that develops decentralized applications, .
