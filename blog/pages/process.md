---
layout: narrow
title: The voting process
description: How does voting with Start.Vote work?
permalink: /process
---

This document explains how the Start.Vote process works.

## Definitions

If you want to build an app using our process, you might want to use our [Typescript types NPM package](https://www.npmjs.com/package/start-vote-types).

* **Ballot**: A session where individuals vote (an election / voting event). A ballot has a title, multiple OptionAddresses, and a list of adresses that are allowed to vote.
* **CA fallback**: The actor that manages the VoteTokens for Voters that did not register in time.
* **Client Application**: The (web-based, javascript) application that creates Votes and Voting Events.
* **EligibilityCodes**: Hashes, created by the Organizer, to give Voters the right to register.
* **Ledger**: A distributed ledger, such as the Waves chain..
* **Organizer**: The actor that starts the voting process and creates the Ballot.
* **OptionAddress**: A public address on a decentralized ledger that represent a specific vote options for some Ballot.
* **Service**: The web service that helps organize the voting process, mostly by providing a front-end application.
* **Swarm**: The group of computers that run Hybridd Nodes.
* **Tokens**: Cryptographic assets, stored in the Ledger.
* **Vote**: A single vote, cast by one person.
* **Voter Address**: A unique crypto address that is only accessible to the voter.
* **Voter**: The individual that casts a Vote. In our case, this will be the Argu Front-end.
* **VoteToken**: A token on the Ledger that gives the right to cast a single Vote.

This section describes the flow of information for the entire process of an Election - from initiation to auditing.

## Organizer initiates Ballot

* The Organizer (who wants to start the election) visits the voting Service, e.g. [Start.Vote](https://start.vote). This service hosts a web application that the Organizer can use to create the Ballot.
* The Organizer enters relevant data (title, OptionAddresses, deadline, etc.) in the Ballot. The Ballot model is described by [`start-vote-types`](https://www.npmjs.com/package/start-vote-types).
* The Organizer enters the list of eligible Voters. This can be a list of e-mail adresses. EligibilityCode are created for every single Voter in the Client Appplication. They are sent to the Swarm and stored decentralized.
* The Organizer has to make sure enough Tokens are present at the `organizerAddress`. The Service might help with this by providing a place to buy tokens.
* The Service validates and stores the Ballot centralized (decentralized is optional later).
* The Service sends invitations to the Voters by e-mail. These e-mails contain a registration link (to any Hybridd swarm node) with a voting engine function (e.g. somenode/engine/voting/)

## User creates public / private keypair

* The Voter receives the invitation to register for the Ballot. This links to a specific node (e.g. node.start.vote).
* The Voter opens uses a front-end application hosted by the Swarm to create a seed, which can be used to generate a PublicAddress.
* The Voter has the option to back-up the generated seed on his local machine.
* The PublicAddress, combined with the EligibilityCode, is securely sent to the Swarm using the Front-end. (Could be splitted in future to minimize impact malicious nodes)
* The Swarm stores the PublicAddress and couples is to a random EligibilityCode. This way, the swarm does not store a link between Voter and PublicAddress, but it does guarantee that a Voter is eligible.

<!-- If the Organizer specified a CA fallback, an unregistered Voter (without a key pair) can still vote:

* The Organizer sends the unused VoteTokens to the CA fallback address.
* When the Voter signs to the service controlled by the CA fallback, voting, the user still has to create a key pair.
* Service sends the VoteToken to the user. -->

## Organizer sends VoteTokens

* The Swarm returns public keys that are made by the Voters, thereby acting as a 'mixer'. This is the core of the anonimization process, since the link between specific addresses and Voters is severed.
* The Organizer (using Hybridd.interface) collects a set of pubkeys. The pubkeys belong to Voters, but are not traceable to the voters themselves, because the Swarm acts as a privacy intermediary.
* The Organizer (using Hybridd.interface) distributes the VoteTokens.
* The Organizer sends invites and instructions to all the Voters with a link to the Ballot. The invite should contain the ID of the Ballot.

## Voter places Vote

* The Voter is reminded to place the vote. He receives instructions and a link to the Ballot from the Service.
* The Voter enters his seed (perhaps using local storage).
* The seed produces the necessary private and public keys for voting.
* The service provides the Voter with a set of public keys that represent the vote options.
* Users casts vote by sending his VoteToken to the chosen OptionAddress. A deterministic transaction is created and injected into the Ledger by Swarm.

## Get vote results

* The balance of tokens is counted on the various options, using the IOC API.
* The user sends a request to the VAAS service, which calculates the percentages. If a user wants to know the exact

## (Optional) Organizer shares private vote options key

If the Organizer has decided to encrypt the vote options to prevent polling effects on the vote results, the Organizer needs to share his private key with the Swarm in order to make the vote options readable to all others.

The Organizer opens the votingEvent page and clicks the "decrypt vote options results" button and uploads his private key.
