/**
 * A vote option on a ballot.
 */
export interface OptionUnconfirmed {
  /** Local ID, useful in front end. */
  id: number;
  /** E.g. a candidate name or 'yes' or 'no'. */
  title: string;
  /** The public address that represents the Vote Option */
  address?: string;
}

/**
 * A Ballot describes the data required for starting an election.
 * This version does not include permanent URLs or IDs
 */
export interface BallotUnconfirmed {
  /** The public key for the organized. It's used for creating, storing and distributing the VoteRights. */
  organizerAddress?: string;
  /** What is being voted on? Give some context */
  description: string;
  /** Title of the voting event */
  title: string;
  /** What are the options where people can vote on? E.g. candidates or 'yes' and 'no' */
  options: OptionUnconfirmed[];
  /** List of *hashed* identities of voters. */
  eligibilities: string[];
  /** When voting will be closed. */
  deadline: string;
  /** When voting will be enabled. */
  openDate?: string;
  /** If this is true, users don't have to register. This makes the voting process far less anonymous. */
  simpleProcess?: boolean;
  /** Enables hiding all the results until after the election. */
  publicHideKey?: string;
  /** The unique identifier of the Ballot. Should be complex, e.g. SHA-256  */
  ballotId?: string;
}

/**
 * Ballots as stored in the backend.
 */
export interface Ballot extends BallotUnconfirmed {
  _id: string;
  /** Should link directly to the page where the vote can be seen. */
  url: string;
  totalVotes: number;
  optionCounts: Option[];
}

/**
 * A single Vote from a user.
 */
export interface VoteUnconfirmed {
  // Double hashed ID
  doubleHashedId: string;
  ballotId: string;
  option: string;
  // Sender
  voterAddress: string;
  // Receiver
  optionAddress: string;
}

/**
 * A single Vote from a user.
 */
export interface Vote extends VoteUnconfirmed {
  createdAt: string;
  _id: string;
}

/**
 * Ballots as stored in the backend.
 */
export interface Option extends OptionUnconfirmed {
  _id: string;
  count: number;
  /** Every vote option needs an address to where the vote token should be sent to */
}
