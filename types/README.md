# Start.vote types

A set of types used for creating vote events (elections) and votes for the [start.vote](https://start.vote) project.

## Usage

Install using npm: `npm install --save-dev start-vote-types`
Install using yarn: `yarn add -D start-vote-types`
